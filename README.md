### typhon.R : Generate PCR chimeras

In Greek mythology, Typhon is the father of many monsters -- including the chimera. 
typhon.R does essentially the same: Combines two sequences to create articial chimeras, as are often produced by polymerase chain reaction (PCR). These can then be inserted into data sets to evaluate chimera detection techniques.   

#### Quick-start:

    Rscript typhon.R -f example.fasta 

##### Options:
	-f FILE, --file=FILE
		path to fasta containing 2 sequences

	-a, --fixed_len
		generate chimeras of a fixed length, that of the larger sequence [default TRUE]

	-b, --variable_len
		generate chimeras of variable length (nested-loop)  [default FALSE]

	-r RIGHT_INTERVAL, --right_interval=RIGHT_INTERVAL
		number of characters to increase by for right sequence [default 1]

	-l LEFT_INTERVAL, --left_interval=LEFT_INTERVAL
		number of characters to increase by for left sequence [default 1]

	-s RIGHT_START_INDEX, --right_start_index=RIGHT_START_INDEX
		positional index to start from for right sequence [default 1]

	-t LEFT_START_INDEX, --left_start_index=LEFT_START_INDEX
		positional index to start from for left sequence [default 1]

	-q RIGHT_STOP_INDEX, --right_stop_index=RIGHT_STOP_INDEX
		positional index to stop at for right sequence [default is length of right seq]

	-v LEFT_STOP_INDEX, --left_stop_index=LEFT_STOP_INDEX
		positional index to stop at for left sequence [default is length of left seq]

	-h, --help
		Show this help message and exit

#### Output:
The chimeras are printed to STDOUT in fasta format. 
The fasta header contains the sequence IDs of both sequences as well as the number
of base-pairs included from each. 

This information is provided in two ways: 

- The two sequence headers and base-pair counts are separated by a '__' 

- The sequence headers and base-pair counts are printed as key-value pairs in the manner used by obitools to allow further processing with obitools.

The obitools-style key-value pairs are these:
* 'right_seq' : the "right" sequence header.
* 'left_seq'  : the "left" sequence header.
* 'right_len' : the number of basepairs that came from the "right" sequence.
* 'left_len'  : the number of basepairs that came from the "left" sequence.

#### Parallel Example:
